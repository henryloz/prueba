@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Postulantes
                </div>
                
                <div class="card-body">

                  
                    
                    <div class="row">
                        <div class="col-md-6">
                            <a href="{{ route('usuarios.create') }}" class="btn btn-success text-white">Nuevo</a>

                            <a href="" class="btn btn-outline-success">Exportar Excel</a>

                            <a href="" class="btn btn-outline-danger">Exportar PDF></a>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <input type="text" name="usuario" class="form-control" autocomplete ="off">
                                <button type="submit" class="btn btn-primary text-white">Buscar</button>
                            </div>
                        </div>
                        
                    </div>                   
                                 
                
            
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                 <th>#</th>
                                 <th>Nombre</th>                                 
                                 <th>DNI</th>
                                 <th>Especialidad</th>
                                 <th>DNIimg</th>
                                 <th>certificado</th>
                                 <th>pago</th>
                                 <th>editar</th>
                                 <th>eliminar</th>
                            </tr>
                        </thead>
                        
                </div>
            </div>
        </div>
    </div>
</div>
@endsection