@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Bien venidos al sistema del ISTAP
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    USTED TIENE LA OPCIÓN DE UN BUEN FUTURO POSTULANDO A LAS CARRERAS TECNICAS QUE OFRECEMOS
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
