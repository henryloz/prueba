@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    usuarios
                </div>
                
                <div class="card-body">

                  
                    
                    <div class="row">
                        <div class="col-md-6">
                            <a href="{{ route('usuarios.create') }}" class="btn btn-success text-white">Nuevo</a>

                            <a href="" class="btn btn-outline-success">Exportar Excel</a>

                            <a href="" class="btn btn-outline-danger">Exportar PDF></a>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="input-group mb-3">
                                <input type="text" name="usuario" class="form-control" autocomplete ="off">
                                <button type="submit" class="btn btn-primary text-white">Buscar</button>
                            </div>
                        </div>
                        
                    </div>                   
                                 
                
            
                </div>

                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                 <th>#</th>
                                 <th>Nombre</th>
                                 <th>Email</th>
                                 <th>Password</th>
                                 <th>Rol</th>
                                 <th>editar</th>
                                 <th>eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($usuarios as $key => $usuario)
                            <tr>
                                <td>{{ $key+1 }}</td>                            
                                <td>{{ $usuario->name }}</td>
                                <td>{{ $usuario->email }}</td>
                                <td>{{ $usuario->password }}</td>
                                <td>{{ $usuario->rol }}</td>                                
                                <td>
                                    <a href="{{ route('usuarios.edit', $usuario->id) }}" class="btn btn-warning">Editar</a>
                                </td>
                                <td>
                                    <a href="{{ route('usuarios.destroy', $usuario->id) }}" class="btn btn-danger">Eliminar</a>
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection