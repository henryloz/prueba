@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Nuevo usuario
                </div>

                <div class="card-body">
                    <form action="{{ route('usuarios.store') }}" method="POST">
                        @csrf
                        <div class="mb-3">
                            <label for="">Nombre</label>
                            <input type="text" name="name" class="form-control" autocomplete="off" required>
                        </div>                        
                        <div class="mb-3">
                            <label for="">Email</label>
                            <input type="email" name="email" class="form-control" autocomplete="off" required>
                        </div>
                        <div class="mb-3">
                            <label for="">Rol</label>
                            <select name="rol" class="form-select">
                                <option value="">--- seleccionar ---</option>
                                <option value="Estudiante">Estudiante</option>
                                <option value="Administrador">Administrador</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="">password</label>
                            <input type="password" name="password" class="form-control" autocomplete="off" required>
                        </div>
                        <div class="mb-3">
                            <input type="submit" class="btn btn-success" value="Guardar">
                            
                            <a href="{{ route('usuarios.index') }}" class="btn btn-primary">Volver</a>
                        </div>
                    </form>
                </div>

             
            </div>
        </div>
    </div>
</div>
@endsection