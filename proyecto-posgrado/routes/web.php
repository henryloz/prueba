<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\Usuario\UsuarioController;
use App\Http\Controllers\Usuario\PostulanteController;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/usuarios/index', [UsuarioController::class, 'index'])->name('usuarios.index')->middleware('auth');

Route::get('/usuarios/create', [UsuarioController::class, 'create'])->name('usuarios.create');

Route::post('/usuarios/store', [UsuarioController::class, 'store'])->name('usuarios.store');

Route::get('/usuarios/edit/{id}', [UsuarioController::class, 'edit'])->name('usuarios.edit');

Route::put('/usuarios/edit/{usuario}', [UsuarioController::class, 'update'])->name('usuarios.update');

Route::get('/usuarios/destroy/{id}', [UsuarioController::class, 'destroy'])->name('usuarios.destroy');

Route::delete('/usuarios/delete/{usuario}', [UsuarioController::class, 'delete'])->name('usuarios.delete');



Route::get('/postulante/index', [PostulanteController::class, 'index'])->name('postulante.index')->middleware('auth');

